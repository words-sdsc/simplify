#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/assign.hpp>

#include <fstream>
#include <string>

using namespace boost::assign;
using namespace std;
//g++ simplify.cpp -o main && ./main
//clang++ -I /usr/local/include -L /usr/local/lib -lboost_system simplify.cpp -o main && ./main

int main(int argc, char** argv)
{
    string originalFile,simplifyFile;
    if(argc == 3)
    {
      originalFile = argv[1];
      simplifyFile = argv[2];
      cout << "Original file:   " << originalFile << endl;
      cout << "Simplified file: " << simplifyFile << endl;
    }
    else
    {
      cout << "Error: Provide original and/or simplified file name." << endl;
      exit (0);
    }

    typedef boost::geometry::model::d2::point_xy<double> xy;
    boost::geometry::model::linestring<xy> line;
    // Files
    ifstream infile(originalFile);


    double x,y;
    while(infile >> x >> y)
    {
      line += xy(x,y);
    }

    // Simplify it, using distance of 0.5 units
    boost::geometry::model::linestring<xy> simplified;
    boost::geometry::simplify(line, simplified, 0.5);
    ofstream outfile(simplifyFile);
    cout << "Output simplified curve to text file" << endl;
    double x1,y1;
    for(const auto& s : simplified)
    {
      x1 = boost::geometry::get<0>(s);
      y1 = boost::geometry::get<1>(s);
      outfile << x1 << " " << y1 << endl;
    }
    infile.close();
    outfile.close();

    return 0;
}