# simplify

## Run simplify with a line inputted in a text file
### Compile
```
g++ simplify.cpp -o main
```
Or
```
clang++ -I /usr/local/include -L /usr/local/lib -lboost_system simplify.cpp -o main
```

### Run
```
./main line/original.txt line/simplified.txt
```
where original.txt is the list of x-y points (seperate by a space and in two rows) to be simplified
and simplified.txt is the output points that was simplified.

## Run simplify with Maria Fire (WKT format)
This work is exploratory and a way to make sure that the [simplify Boost function](https://www.boost.org/doc/libs/1_50_0/libs/geometry/doc/html/geometry/reference/algorithms/simplify/simplify_3.html)
works with one of our fire perimeters. This supports any shapefile that was converted
with ogr2ogr from the command line to WKT format 
(`ogr2ogr -f CSV out.wkt ignition.shp -lco GEOMETRY=AS_WKT`). For illustration purposes,
we are using the Maria fire ignition polygon which only have one fire perimeter.

### Compile
```
g++ simplifyWKT.cpp -o simplifyWKT
```
Or
```
clang++ -I /usr/local/include -L /usr/local/lib -lboost_system simplify.cpp -o simplifyWKT
```

### Run
```
./simplifyWKT maria/ignition.csv 20.00 maria/original.txt \
maria/simplified20.00.txt maria/simplified20.00.csv
```
Command line arguments:
- `ignition.csv` (input): WKT polygon in a CSV file (assumes that there is only one fire perimeter)
- `20.0` (input): corresponds to the tuning parameter for simplifying perimeter
- `original.txt` (output): the output list of x-y points (seperate by a space and in two rows) to be simplified
- `simplified20.00.txt` (output): the output points that were simplified
- `mariaSimplified20.00.csv` (output): simplified vector layer in wkt format in a csv file

### Run Bash script
The bash script will compile and run a series of test with different tuning parameter that is meant to reduce
the number of points on the fire perimeter.
```
bash wktRuns.sh
```
The output will be timings and a bunch of simplified text files that are meant to be used to
input to the jupyter notebook.

Example output timing:
```
Simplify: c = 0.250000, Points = 383, Elapsed Time = 0.0024 sec
Simplify: c = 0.500000, Points = 383, Elapsed Time = 0.0023 sec
Simplify: c = 0.750000, Points = 378, Elapsed Time = 0.0027 sec
Simplify: c = 1.000000, Points = 376, Elapsed Time = 0.0024 sec
Simplify: c = 1.250000, Points = 376, Elapsed Time = 0.0024 sec
Simplify: c = 1.500000, Points = 373, Elapsed Time = 0.0029 sec
Simplify: c = 5.000000, Points = 322, Elapsed Time = 0.0024 sec
Simplify: c = 10.000000, Points = 272, Elapsed Time = 0.0027 sec
Simplify: c = 20.000000, Points = 206, Elapsed Time = 0.0024 sec
```
The timing results shows that the function is constant while decreasing the number of points.

## Jupyter notebook
The jupyter notebook (`checkOutput.ipynb`) will visualize all the results with the .txt files
outputted by `bash wktRuns.sh`.

## Convert CSV output file to shape file
In order to get a shape file, the .csv file outputted by `simplifyWKT` must be converted
with `ogr2ogr` with the following command:
```
ogr2ogr -f "ESRI Shapefile" maria/simplified.shp maria/simplified.csv
```
Furthermore, the original projection file (.prj) needs to be copied
with the same name as the .csv file (i.e `simplified.prj`).
```
cp maria/ignition.prj maria/simplified.prj
```

## Full run
To run a full simplified workflow (including all the files conversions) run the following
command:
```
bash wkt1FullRun.sh
```