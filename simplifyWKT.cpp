#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <iostream>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/assign.hpp>

#include <fstream>
#include <string>
#include <chrono>

using namespace boost::assign;
using namespace std;

int main(int argc, char** argv)
{
    string originalFile,simplifyFile;
    string wktCSVFile,ignitionFile;
    double tuningParam;
    if(argc == 6)
    {
      //Input
      ignitionFile = argv[1];
      tuningParam = atof(argv[2]);
      //Output files
      originalFile = argv[3];
      simplifyFile = argv[4];
      wktCSVFile = argv[5];
    }
    else
    {
      cout << "Error: Not enough input arguments." << endl;
      cout << "Example: ./simplifyWKT ignition.csv out1.txt out2.txt 10.0 simple.csv" << endl;
      exit (0);
    }
    typedef boost::geometry::model::d2::point_xy<double> xy;
    boost::geometry::model::polygon<xy> mariaFire;
    ifstream file(ignitionFile);
    vector<string> dataList;
    string line = "";
    size_t pos;
    string formatLine,header;
    string polygon = "POLYGON";
    while (getline(file, line))
    {
      //read header
      if (line.find("WKT") != string::npos)
      {
        //get header
        header = line;
      }

      if (line.find(polygon) != string::npos)
      {
        //Stores only wkt
        pos = line.find("\",");
        formatLine = line.erase(pos,line.size()-pos);
        formatLine = formatLine.erase(0,1);
        dataList.push_back(formatLine);
      }
    }
    file.close();
    boost::geometry::read_wkt(dataList[0], mariaFire);
    boost::geometry::model::polygon<xy> simplified;
    
    //Timing
    auto start = chrono::high_resolution_clock::now();
    boost::geometry::simplify(mariaFire, simplified, tuningParam);
    auto end = chrono::high_resolution_clock::now();
    // Calculating total time taken by the program. 
    double time_taken = chrono::duration_cast<chrono::nanoseconds>(end - start).count(); 
    time_taken *= 1e-9; 
    uint sizeOfSimplified = boost::geometry::num_points(simplified);
    cout << fixed;
    cout << "Simplify: c = " << tuningParam << setprecision(4);
    cout << ", Points = " << sizeOfSimplified;
    cout << ", Elapsed Time = "<< time_taken;
    cout << " sec" << endl; 
    //Output original
    ofstream outfile1(originalFile);
    double x1,y1;
    for(auto it = boost::begin(boost::geometry::exterior_ring(mariaFire));
      it != boost::end(boost::geometry::exterior_ring(mariaFire)); ++it)
    {
        x1 = boost::geometry::get<0>(*it);
        y1 = boost::geometry::get<1>(*it);
        outfile1 << x1 << " " << y1 << endl;
    }
    outfile1.close();
    ofstream outfile(simplifyFile);
    for(auto it = boost::begin(boost::geometry::exterior_ring(simplified));
      it != boost::end(boost::geometry::exterior_ring(simplified)); ++it)
    {
        x1 = boost::geometry::get<0>(*it);
        y1 = boost::geometry::get<1>(*it);
        outfile << x1 << " " << y1 << endl;
    }
    outfile.close();

    //Output the new wkt in csv
    ofstream outWKT(wktCSVFile);
    outWKT << header << endl;
    outWKT << "\"";
    outWKT << fixed;
    outWKT << boost::geometry::wkt(simplified)<< setprecision(12);
    outWKT << "\",\"0\"" << endl;
    return 0;
}