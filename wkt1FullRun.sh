#!/bin/bash
echo 'Compile'
g++ simplifyWKT.cpp -o simplifyWKT
echo 'Convert ignition.shp to WKT (in csv file)'
ogr2ogr -f CSV maria/out.wkt maria/ignition.shp -lco GEOMETRY=AS_WKT
echo 'Copy ignition.csv to maria/ directory'
cp maria/out.wkt/ignition.csv maria/ignition.csv
rm -rf maria/out.wkt
echo 'Run SimplifyWKT'
./simplifyWKT maria/ignition.csv 20.0 maria/original.txt \
maria/simplified20.0.txt maria/simplified20.0.csv
echo 'Convert WKT from csv file to Shape File'
ogr2ogr -f "ESRI Shapefile" maria/simplified20.0.shp maria/simplified20.0.csv
echo 'Copy projection file'
cp maria/ignition.prj maria/simplified20.0.prj
